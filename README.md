# README #
Creado por Irene Martín-Maestro , Irene Mengual Mahave y Stella Moral Salguero.


### What is this repository for? ###

* Máquina expendedora de cafés.
* 05/02/2021


### Breve resumen de la aplicación ##

* El usuario inserta monedas de una en una hasta llegar al precio de 50 céntimos.
* La máquina admite monedas de 10.20 y 50 céntimos.
* En caso de exceder el importe,éste sería devuelto.
* Se mostrará por pantalla la cuenta de monedas para facilitar su uso.
* Una vez alcanzado el precio requerido para el café ( 50 céntimos) , el usuario escoge el café que desee.
* Opciones de café posibles: café solo , café con leche o capuchino.
* Una vez seleccionado el café, se enciende la bomba de compresión y cae el café, la leche o el chocolate en función de la selección.
