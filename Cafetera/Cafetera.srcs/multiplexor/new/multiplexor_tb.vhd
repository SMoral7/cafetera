library ieee;
use ieee.std_logic_1164.all;

entity tb_MULTIPLEXOR is
end tb_MULTIPLEXOR;

architecture tb of tb_MULTIPLEXOR is

    component MULTIPLEXOR
        port (RESET_N         : in std_logic;
              CLK             : in std_logic;
              display         : out std_logic_vector (6 downto 0);
              STATE           : in std_logic_vector (0 to 2);
              current_display : out std_logic_vector (3 downto 0));
    end component;

    signal RESET_N         : std_logic;
    signal CLK             : std_logic;
    signal display         : std_logic_vector (6 downto 0);
    signal STATE           : std_logic_vector (0 to 2);
    signal current_display : std_logic_vector (3 downto 0);

    constant TbPeriod : time := 2ms; 
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : MULTIPLEXOR
    port map (RESET_N         => RESET_N,
              CLK             => CLK,
              display         => display,
              STATE           => STATE,
              current_display => current_display
    );


    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';
    CLK <= TbClock;

    stimuli : process
    begin
        RESET_N <= '0' after 0.25 * TbPeriod, '1' after 0.75 * TbPeriod;
        STATE <= "000";
        wait for 10ms;
        STATE <="001";
        wait for 10ms;
        STATE <="010";
        wait for 10ms;
        STATE <="011";
        wait for 10ms;
        STATE <="100";
        wait for 10ms;
        STATE <="101";
        wait for 10ms;
        STATE <="110";
        wait for 10ms;
        STATE <="111";
        wait for 10ms;
        
        wait for 100 * TbPeriod;

        TbSimEnded <= '1';
        wait;
    end process;

end tb;



