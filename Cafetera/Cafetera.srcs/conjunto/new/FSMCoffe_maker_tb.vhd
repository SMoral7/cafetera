library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity coffe_maker_tb is
end coffe_maker_tb;

architecture Behavioral of coffe_maker_tb is

component coffe_maker 
port (
     coins_ok: in std_logic;

     RESET : in std_logic;-- este reset te devuelve el dinero directamente
     CLK : in std_logic;
     PUSHBUTTON : in std_logic_vector(0 TO 2); 
     terminado_ok: out std_logic; 
     LIGHT : out std_logic_vector(0 TO 2);
     LIGHT_depositos : out std_logic_vector(0 TO 2); --0 cafe, 1 leche, 2 chocolate
     LIGHT_BOMBA: out std_logic

     );
end component;

--Inputs
  signal reset     : std_logic :='1';
  signal clk       : std_logic;
  signal pushbutton: std_logic_vector(0 TO 2); 
  signal coins_ok  : std_logic;
  signal terminado_ok: std_logic; 
  SIGNAL LIGHT_depositos :  std_logic_vector(0 TO 2); --0 cafe, 1 leche, 2 chocolate
  signal LIGHT_BOMBA: std_logic;


  --Outputs
  signal light     :  std_logic_vector(0 TO 2);

  -- Clock period definitions
  constant clk_period: time := 10 ns;
  constant clk_period_1s : time := 1000 ns;
begin

uut: coffe_maker 
    port map(
         coins_ok=> coins_ok,
         RESET =>RESET,
         CLK =>CLK,
         PUSHBUTTON =>PUSHBUTTON,
         LIGHT => LIGHT,
         terminado_ok => terminado_ok,
         LIGHT_depositos=>LIGHT_depositos,
         LIGHT_BOMBA=>LIGHT_BOMBA
         );

clk_process :process
  begin
    clk <= '1';
    wait for 0.5 * clk_period;
    clk <= '0';
    wait for 0.5 * clk_period;
 end process; 
  
reset <= '0' after 0.25 * clk_period_1s, '1' after 0.75 * clk_period_1s;


coins_ok <= '0' ,
            '1' after 0.25* clk_period_1s,
            '0' after 22*clk_period_1s, 
            '1' after 23* clk_period_1s,
            '0' after 58*clk_period_1s, 
            '1' after 59* clk_period_1s;
            
PUSHBUTTON <= "000",
              "100" after 0.5* clk_period_1s,
              "000" after 1.5* clk_period_1s,
              "010" after 25* clk_period_1s,
              "000" after 26* clk_period_1s,
              "001" after 61* clk_period_1s,
              "000" after 62* clk_period_1s;
              


assert false
report "Fin de la simulacion..."
severity failure;


end Behavioral;