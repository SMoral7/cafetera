library ieee;
use ieee.std_logic_1164.all;

entity FSM_MONEDAS_TB is
end FSM_MONEDAS_TB;

architecture TEST of FSM_MONEDAS_TB is

    procedure wait_for_n_ticks(ticks : positive; signal clk : std_logic) is
    begin
        for i in 1 to ticks loop
            wait until clk = '1';
        end loop;
    end procedure;

    procedure pulse_for_one_tick(signal s : inout std_logic; signal clk : std_logic; negated : boolean := false) is
        variable s_prv : std_logic;
    begin
        wait until clk = '1';
        s_prv := s;
        if negated then
            s <= '0';
        else 
            s <= '1';
        end if;
        wait until clk = '1';
        s <= s_prv;
    end procedure;

    component FSM_monedas
        generic (
            CLK_FREQ : positive;
            T_FAIL   : time
        );
        port (
            RESET_MONEDA : in  std_logic;
            CLK          : in  std_logic;
            PUSHBUTTON   : in  std_logic_vector(0 TO 2);
            RESTART      : in  std_logic;
            LIGHT        : out std_logic_vector(0 TO 1);
            COINS_OK_50C : out std_logic;
            STATE        : out std_logic_vector(0 TO 2)
        );
    end component;

    -- Inputs
    signal reset_moneda : std_logic;
    signal clk          : std_logic;
    signal restart      : std_logic;
    signal cents10      : std_logic;
    signal cents20      : std_logic;
    signal cents50      : std_logic;

    -- Outputs
    signal light        : std_logic_vector (0 to 1);
    signal coins_ok_50c : std_logic;
    signal state        : std_logic_vector (0 to 2);

    constant TbFreq   : positive := 1_000; -- 1kHz 
    constant TbPeriod : time := 1 sec / TbFreq;
 
    signal TbClock    : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

    signal pushbutton   : std_logic_vector (0 to 2);

begin      
    pushbutton <= (cents50, cents20, cents10);

    dut : FSM_MONEDAS
        generic map (
            CLK_FREQ => TbFreq,
            T_FAIL   => 100 ms
        )
        port map (
            RESET_moneda => reset_moneda,
            CLK          => clk,
            PUSHBUTTON   => pushbutton,
            restart      => restart,
            LIGHT        => light,
            COINS_OK_50C => coins_ok_50c,
            STATE        => state
        );

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- Reset
        reset_moneda <= '1';
        pulse_for_one_tick(reset_moneda, clk, true);

        -- Input initalization
        restart <= '0';
        cents10 <= '0';
        cents20 <= '0';
        cents50 <= '0';

        --Caso 1: meter 50c
        pulse_for_one_tick(cents50, clk);
        wait_for_n_ticks(5, clk);
        pulse_for_one_tick(restart, clk);
        wait_for_n_ticks(5, clk);
  
        --Caso 2: meter de m�s: 60c
        pulse_for_one_tick(cents20, clk);
        wait_for_n_ticks(5, clk);
        pulse_for_one_tick(cents50, clk);
        wait for 110 ms;
        pulse_for_one_tick(restart, clk);
        wait_for_n_ticks(5, clk);
        
        --Caso 3: meter 20c+10c+10c+10c;
        pulse_for_one_tick(cents20, clk);
        wait_for_n_ticks(5, clk);
        pulse_for_one_tick(cents10, clk);
        wait_for_n_ticks(5, clk);
        pulse_for_one_tick(cents10, clk);
        wait_for_n_ticks(5, clk);
        pulse_for_one_tick(cents10, clk);
        wait_for_n_ticks(5, clk);
        pulse_for_one_tick(restart, clk);
        wait_for_n_ticks(5, clk);

        wait_for_n_ticks(5, clk);

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end TEST;