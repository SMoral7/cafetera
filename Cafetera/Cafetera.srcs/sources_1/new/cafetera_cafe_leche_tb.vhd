library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cafetera_cafe_leche_tb is
end cafetera_cafe_leche_tb;

architecture Behavioral of cafetera_cafe_leche_tb is
    component cafetera is port (  
--CENTIMOS     
     button_coins: in std_logic_vector (0 to 2); --100 50c, 010 20c, 001 10c
     
 --SELECCI�N DE CAFES
     button_coffes: in std_logic_vector (0 to 2); --100 cSolo, 010 cLeche, 001 cCapuchino


     CLK : in std_logic; --100MHZ 
     RESET_N: in std_logic; 
     LIGHT_coins : out std_logic_vector(0 TO 1);--10 50c, 01 devuelve dinero
     LIGHT_coffes : out std_logic_vector(0 TO 2); --100 cSolo, 010 cLeche, 001 cCapuchino
     LIGHT_display : out std_logic_vector(0 TO 6);
     LIGHT_current_display : out std_logic_vector(0 TO 3);
     LIGHT_deposits : out std_logic_vector(0 TO 2); ----100 caf�, 010 lche, 001 chocolate
     LIGHT_bomba : out std_logic --bomba de dep�sitos


);
     end component;
     
 --inputs
signal button_coins: std_logic_vector (0 to 2);
signal button_coffes:  std_logic_vector (0 to 2); 
signal CLK :  std_logic; --100MHZ 
signal RESET:  std_logic;

--outputs
signal LIGHT_coffes : std_logic_vector(0 TO 2);
signal LIGHT_coins :  std_logic_vector(0 TO 1);--SE ENCIENDE EL LED0 SI tiene 50 centimos-> simula accion BOMBA, led 1 si RESET o se ha pasado de dinero--> simula accion DEVUELVE DINERO
signal LIGHT_deposits :  std_logic_vector(0 TO 2);
signal LIGHT_bomba :  std_logic ;
signal LIGHT_display : std_logic_vector(0 TO 6);
signal LIGHT_current_display : std_logic_vector(0 TO 3);

constant clk_period: time := 10 ns;
constant clk_period_1s : time := 1000 ms;



begin
        inst_cafetera: cafetera
        port map(
             button_coins => button_coins,
             button_coffes => button_coffes,
             CLK => clk, --100MHZ 
             RESET_n => reset,
             LIGHT_coins => LIGHT_coins,--SE ENCIENDE EL LED0 SI tiene 50 centimos-> simula accion BOMBA, led 1 si RESET o se ha pasado de dinero--> simula accion DEVUELVE DINERO
             LIGHT_coffes => LIGHT_coffes,
             LIGHT_deposits => LIGHT_deposits,
             LIGHT_bomba => LIGHT_bomba,
             LIGHT_display => LIGHT_display,
             LIGHT_current_display => LIGHT_current_display
        
        );

clk_process :process
  begin
    clk <= '1';
    wait for 0.5 * clk_period;
    clk <= '0';
    wait for 0.5 * clk_period;
 end process; 
  
reset <= '0' after 0.25 * clk_period_1s, 
         '1' after 0.75 * clk_period_1s;


button_coffes <= "000",
                 "010" after 2* clk_period_1s,
                 "000" after 3* clk_period_1s;
            
button_coins <= "000",
                 "100" after 0.5* clk_period_1s,
                 "000" after 1.5* clk_period_1s;
              


assert false
report "Fin de la simulacion..."
severity failure;
end behavioral; 
