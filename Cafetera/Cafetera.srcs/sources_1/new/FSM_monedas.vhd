library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity FSM_MONEDAS is
    generic (
        CLK_FREQ : positive := 100_000_000;  -- Nexys4 100MHz 
        T_FAIL   : time := 5 sec
    );
    port (
        RESET_MONEDA : in  std_logic;-- este reset te devuelve el dinero directamente
        CLK          : in  std_logic;
        PUSHBUTTON   : in  std_logic_vector(0 TO 2); --BOTONES simulan las monedas: B0 (10c), B1(20c), b2(50c)
        RESTART      : in  std_logic;
        LIGHT        : out std_logic_vector(0 TO 1);--SE ENCIENDE EL LED0 SI ESTA EN S5, EL LED1 SI ESTA EN S6
        -- LIGHT 0 SIMULA ACCION BOMBA, LIGHT 1 SIMULA ACCION DEVUELVE DINERO
        COINS_OK_50C : out std_logic;
        STATE        : out std_logic_vector(0 TO 2)--Vector que se devuelve en base al estado
    );
end FSM_MONEDAS;

architecture BEHAVIORAL of FSM_MONEDAS is

    constant T_FAIL_TICKS : positive := CLK_FREQ * T_FAIL / 1 sec - 1;
 
    type STATES is (S0, S1, S2, S3, S4, S5, S6); --s0:0c, s1:10c, s2:20c, s3:30c, s4:40c, s5:50c, s6: DEVUELVEDINERO
    signal current_state : STATES;
    signal next_state    : STATES;

    subtype ticks_t is integer range 0 to T_FAIL_TICKS;
    signal remainig_ticks, nxt_remainig_ticks: ticks_t; 

begin
    state_register: process (RESET_MONEDA, CLK)
    begin
        if RESET_MONEDA = '0' then 
            current_state  <= S0;
            remainig_ticks <= 0;
        elsif rising_edge(CLK) then
            current_state  <= next_state;
            remainig_ticks <= nxt_remainig_ticks;
        end if;
    end process;

    nextstate_decod: process (current_state, remainig_ticks, PUSHBUTTON, RESTART)
    begin
        next_state         <= current_state;
        nxt_remainig_ticks <= remainig_ticks;

        case current_state is
            when S0 => --hay 0c
                if PUSHBUTTON = "001" then --meto 10c
                    next_state <= S1;
                elsif PUSHBUTTON = "010" then --meto 20c
                    next_state <= S2;
                elsif PUSHBUTTON = "100" then --meto 50c
                    next_state <= S5;
                end if;
                
            when S1 => --hay 10c
                if PUSHBUTTON = "001" then --meto 10c
                    next_state <= S2;
                elsif PUSHBUTTON = "010" then --meto 20c
                    next_state <= S3;
                elsif PUSHBUTTON = "100" then --meto 50c
                    nxt_remainig_ticks <= T_FAIL_TICKS;
                    next_state <= S6;
                end if;
                
            when S2 => --hay 20c
                if PUSHBUTTON = "001" then --meto 10c
                    next_state <= S3;
                elsif PUSHBUTTON = "010" then --meto 20c
                    next_state <= S4;
                elsif PUSHBUTTON = "100" then --meto 50c
                    next_state <= S6;
                    nxt_remainig_ticks <= T_FAIL_TICKS;
                end if;
                
            when S3 => --hay 30c
                if PUSHBUTTON = "001" then --meto 10c
                    next_state <= S4;
                elsif PUSHBUTTON = "010" then --meto 20c
                    next_state <= S5;
                elsif PUSHBUTTON = "100" then --meto 50c
                    next_state <= S6;
                    nxt_remainig_ticks <= T_FAIL_TICKS;
                end if;
                
            when S4 => --hay 40c
                if PUSHBUTTON = "001" then --meto 10c
                    next_state <= S5;
                elsif PUSHBUTTON = "010" then --meto 20c
                    next_state <= S6;
                    nxt_remainig_ticks <= T_FAIL_TICKS;
                elsif PUSHBUTTON = "100" then --meto 50c
                    next_state <= S6;
                    nxt_remainig_ticks <= T_FAIL_TICKS;
                end if;
                
            when S5 => --hay 50c
                if restart = '1' then 
                    next_state <= S0;
                end if; 

            when S6 => --hay 50c
                if remainig_ticks /= 0 then
                    nxt_remainig_ticks <= remainig_ticks - 1;
                else
                    next_state <= S0;     
                end if;
                
            when others =>
                nxt_remainig_ticks <= T_FAIL_TICKS;
                next_state <= S6;
        end case;
    end process;

    output_decod: process (current_state, remainig_ticks)
    begin
        LIGHT        <= (others => '0');
        COINS_OK_50C <= '0';

        case current_state is
            when S0 =>
                STATE <= "000";

            when S1 =>
                STATE <= "001";

            when S2 =>
                STATE <= "010";

            when S3 =>
                STATE <= "011";

            when S4 =>
                STATE <= "100";

            when S5 =>
                STATE <= "101";
                LIGHT <= "10";
                COINS_OK_50C <= '1';

            when S6=>
                STATE <= "110";
                LIGHT <= "01";

            when others =>
                STATE <= "111";
        end case;
    end process;
end architecture BEHAVIORAL;