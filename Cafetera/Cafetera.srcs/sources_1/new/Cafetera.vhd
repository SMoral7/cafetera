library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Cafetera is
port (
--CENTIMOS     
     button_coins: in std_logic_vector (0 to 2); --100 50c, 010 20c, 001 10c
     
 --SELECCI�N DE CAFES
     button_coffes: in std_logic_vector (0 to 2); --100 cSolo, 010 cLeche, 001 cCapuchino


     CLK : in std_logic; --100MHZ 
     RESET_N: in std_logic; 
     LIGHT_coins : out std_logic_vector(0 TO 1);--10 50c, 01 devuelve dinero
     LIGHT_coffes : out std_logic_vector(0 TO 2); --100 cSolo, 010 cLeche, 001 cCapuchino
     LIGHT_display : out std_logic_vector(0 TO 6);
     LIGHT_current_display : out std_logic_vector(0 TO 3);
     LIGHT_deposits : out std_logic_vector(0 TO 2); ----100 caf�, 010 lche, 001 chocolate
     LIGHT_bomba : out std_logic --bomba de dep�sitos
     

);

end Cafetera;

architecture Behavioral of Cafetera is
--botones de coins
     signal sync_out_coins: std_logic_vector(button_coins'range);
     signal edge_out_coins: std_logic_vector(button_coins'range);
     
--botones de seleccion
     signal sync_out_coffe: std_logic_vector(button_coffes'range);
     signal edge_out_coffe: std_logic_vector(button_coffes'range);
     
--se�al que sale de la FSM y entra en la cafetera
    signal coins_ok_50c: std_logic;
    signal terminado_ok: std_logic; 
    
--se�al state que sale de la FSM y entra en el mux
    signal STATES: std_logic_vector(0 TO 2);
    
--se trabaja con 1Khz
    component prescaler is 
    generic ( relacion: integer := 500000);--50000000
    PORT ( clk : in  STD_LOGIC; -- 100 MHz
           reset : in  STD_LOGIC;
           clk_out : out  STD_LOGIC
           );
    end component;
    --reloj 1Hz
     signal CLK_1Khz: std_logic;
--COMPONENTES PARA LOS BOTONES    
     component SYNCHRNZR is port(
         CLK : in std_logic; 
         ASYNC_IN : in std_logic; 
         SYNC_OUT : out std_logic);
     end component;
     
     component EDGEDTCTR is port(
         CLK : in std_logic; 
         SYNC_IN : in std_logic; 
         EDGE : out std_logic);
     end component;
     
     component FSM_monedas 
        port(
            RESET_moneda : in std_logic;
            CLK : in std_logic;
            PUSHBUTTON : in std_logic_vector(0 TO 2); 
            restart: in std_logic;
            LIGHT : out std_logic_vector(0 TO 1);
            STATE:  out std_logic_vector(0 TO 2);
            coins_ok_50c: out std_logic
         );
      end component;
      
      component MULTIPLEXOR 
        port(
            RESET_N: in std_logic;
            CLK: in std_logic;
            display: out std_logic_vector(6 downto 0);
            STATE:  in std_logic_vector(0 TO 2);
            current_display: out std_logic_vector(3 downto 0)
         );
      end component;
     
     component coffe_maker is port(
         coins_ok: in std_logic;
         RESET : in std_logic;-- este reset te devuelve el dinero directamente
         CLK : in std_logic;
         PUSHBUTTON : in std_logic_vector(0 TO 2); 
         terminado_ok: out std_logic; 
         LIGHT : out std_logic_vector(0 TO 2); --0 CafeSolo 1 CafeLeche 2 CafeCapuchino
         LIGHT_depositos : out std_logic_vector(0 TO 2); --0 cafe, 1 leche, 2 chocolate
         LIGHT_BOMBA: out std_logic
         );
     end component;
     
begin

    --componente
    inst_prescaler_1Hz: prescaler
     port map (
                clk      => clk,
                reset    => RESET_N,
                clk_out  => clk_1KHz
            );   
            
   sincronizadores_coins: for i in button_coins'range generate
      inst_synchrnzr_i: SYNCHRNZR
            port map (
                clk      => clk_1KHz,
                async_in => button_coins(i),
                sync_out => sync_out_coins(i)
            );
        inst_edgedtctr_i: edgedtctr
            port map (
                clk         => clk_1KHz,
                sync_in     => sync_out_coins(i),
                edge        => edge_out_coins(i)
            );
    end generate;
    
   inst_fsm_monedas: FSM_monedas
            port map(
                RESET_moneda => RESET_N,
                CLK          => clk_1KHz,
                PUSHBUTTON   => edge_out_coins,
                restart      => terminado_ok,
                LIGHT        => LIGHT_coins,
                STATE        => STATES,
                coins_ok_50c => coins_ok_50c
            );
     
     inst_multiplexor : MULTIPLEXOR
            port map (
                RESET_N         => RESET_N,
                CLK             => CLK,
                display         =>  LIGHT_display,
                STATE           => STATES,
                current_display =>  LIGHT_current_display 
            );
     
            
--para la eleccion de cafes 
    sincronizadores_cafes: for i in button_coffes'range generate
        inst_synchrnzr_i: SYNCHRNZR
            port map (
                clk      => clk_1KHz,
                async_in =>button_coffes(i),
                sync_out => sync_out_coffe(i)
            );
        inst_edgedtctr_i: edgedtctr
            port map (
                clk         => clk_1KHz,
                sync_in     => sync_out_coffe(i),
                edge        => edge_out_coffe(i)
            );
    end generate;
 --CAFETERA
 
    inst_coffe_maker: coffe_maker
        port map(
             coins_ok   => coins_ok_50c,
             RESET      => RESET_N,-- este reset te devuelve el dinero directamente
             CLK        => clk_1KHz,
             PUSHBUTTON => button_coffes, 
             terminado_ok => terminado_ok,
             LIGHT      => LIGHT_coffes,
             LIGHT_depositos => light_deposits,
             LIGHT_BOMBA     => light_bomba
        );
        
end Behavioral;