library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity coffe_maker is
port (
     RESET : in std_logic;-- este reset te devuelve el dinero directamente
     CLK : in std_logic;
     coins_ok: in std_logic;
     PUSHBUTTON : in std_logic_vector(0 TO 2);      
     terminado_ok: out std_logic; 
     LIGHT : out std_logic_vector(0 TO 2); --0 CafeSolo 1 CafeLeche 2 CafeCapuchino
     LIGHT_depositos : out std_logic_vector(0 TO 2); --0 cafe, 1 leche, 2 chocolate
     LIGHT_BOMBA: out std_logic
     );
end coffe_maker;

architecture Behavioral of coffe_maker is
    --pasar a 1Hz de frecuenciA DE RELOJ
    component prescaler is 
    generic ( relacion: integer := 50);--50000000
    PORT ( clk : in  STD_LOGIC; -- 100 MHz
           reset : in  STD_LOGIC;
           clk_out : out  STD_LOGIC
           );
    end component;
    --reloj 1Hz
     signal clk_1Hz: std_logic;
     --FSM
     type STATES is (eleccion_cafe, c_solo,c_leche,c_capuchino, cafe, leche, chocolate, completado ); --s0:0c, s1:10c, s2:20c, s3:30c, s4:40c, s5:50c, s6: DEVUELVEDINERO
     type eleccion is (nada,cafe_solo,cafe_con_leche,cafe_capuchino);
     signal current_state, next_state: STATES;
     signal remainig_ticks, nxt_remainig_ticks: unsigned(7 downto 0);        
     signal cafe_state, nxt_cafe_state: eleccion;
   

begin

    --componente
    inst_prescaler_1Hz: prescaler
     port map (
                clk      => clk,
                reset    => RESET,
                clk_out  => clk_1Hz
            );   
    
    

    election_coffe: process(RESET, clk_1Hz)	
        begin
            if RESET='0' then
                 current_state <= eleccion_cafe;
                 remainig_ticks <= X"00";
                 cafe_state <= nada;
                 
                 
            elsif rising_edge(clk_1Hz) then
                 current_state <= next_state;
                 remainig_ticks<= nxt_remainig_ticks;
                 cafe_state <= nxt_cafe_state;
            end if;
    end process;
    
     nextstate_decod: process (PUSHBUTTON,coins_ok, current_state, cafe_state, remainig_ticks)
      --variable enable: std_logic := '1' ;


      --tiempo de encendido de la bomba
      constant t_encendido_bomba: unsigned(7 downto 0) := X"03";
      --tiempos de cafe: todos el mismo
      constant t_cafe: unsigned(7 downto 0) := X"06";

      constant t_leche_c_leche: unsigned(7 downto 0) := X"05";
      constant t_leche_c_capuchino: unsigned(7 downto 0) := X"03";
      --tiempo de chocolate constante en el capuchino
      
      constant t_chocolate_c_capuchino: unsigned(7 downto 0) := X"04";
      --tiempo en trance de completado
      constant t_completado: unsigned (7 downto 0):= X"02";

      begin
         
        nxt_remainig_ticks<= remainig_ticks - 1;
        next_state <=  current_state;
        nxt_cafe_state <= cafe_state;
         case current_state is
             when eleccion_cafe =>
                 if coins_ok='1' then 
                    if PUSHBUTTON =  "100" then
                            next_state <= c_solo;
                   		    nxt_cafe_state <= cafe_solo;
                   		    nxt_remainig_ticks <= t_encendido_bomba;
                            
                    elsif  PUSHBUTTON =  "010" then
                            next_state <= c_leche;
                            nxt_cafe_state <= cafe_con_leche;
                            nxt_remainig_ticks <= t_encendido_bomba;
                            
                    elsif PUSHBUTTON =  "001" then
                            next_state <= c_capuchino;
                            nxt_cafe_state <= cafe_capuchino;
                            nxt_remainig_ticks <= t_encendido_bomba;
                    else
                         nxt_remainig_ticks <= X"00";
                           
                    end if;   
                                
                  end if; 
                    
             when c_solo =>
                if remainig_ticks=0 then
                    next_state <= cafe;
                    nxt_remainig_ticks <= t_cafe;     
                end if;
                
             when c_leche =>
                if remainig_ticks=0 then
                    next_state <= cafe;
                    nxt_remainig_ticks <= t_cafe;     
                end if;
                
             when c_capuchino =>
                if remainig_ticks=0 then
                    next_state <= cafe;
                    nxt_remainig_ticks <= t_cafe;     
                end if;       
                                         
             when cafe =>
                if remainig_ticks=0 then
                	if cafe_state = cafe_solo then 
                    	next_state <= completado;
                    	nxt_cafe_state<= nada; 
                    else 
                        next_state <= leche;
                    	nxt_remainig_ticks <= t_leche_c_leche;  
                    end if;
                end if;
                
             when leche =>
                if remainig_ticks=0 then
                	if (cafe_state = cafe_con_leche) then 
                    	next_state <= completado;
                    	nxt_cafe_state<= nada;
                    	 
                    else 
                        next_state <= chocolate;
                    	nxt_remainig_ticks <= t_chocolate_c_capuchino;    
                    end if;
                end if;
             
             when chocolate =>
                if remainig_ticks=0 then
                    next_state <= completado;
                    nxt_cafe_state<= nada;
                       
                end if;
             when completado =>
                 next_state <= eleccion_cafe;
                 nxt_remainig_ticks<= X"00";
             when others =>
                next_state <= eleccion_cafe;
                nxt_remainig_ticks<= X"00";
        
         end case;
         end process;
     

    output_decod_cafe_seleccionado: process (cafe_state)
    begin 
    case cafe_state is 
       		when cafe_solo =>
                 LIGHT <="100" ;
                 LIGHT_BOMBA <='1';
             when cafe_con_leche=>
                 LIGHT <="010";
                 LIGHT_BOMBA <='1';
             when cafe_capuchino =>
                 LIGHT <="001";
                 LIGHT_BOMBA <='1';
             when others =>
                LIGHT <= (OTHERS => '0');
             	LIGHT_BOMBA <='0';
        end case;
    end process; 
    
 terminado_ok <= '1' when current_state = completado else
                 '0';
 
    output_decod_depositos: process (current_state)
         begin         
         case current_state is
             when cafe =>
                LIGHT_depositos <= "100";
             when leche =>
                LIGHT_depositos <= "010";
             when chocolate =>
                LIGHT_depositos <= "001";
             when others=>
                 LIGHT_depositos<=(others=>'0');
       end case;
end process;

end Behavioral;