library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity decimal_cntr is
    PORT ( 
     CLR : in std_logic;  --Asynchronous reset
     CLK: in std_logic;   --  clock
     CE : in std_logic;   --Active high clock enable
     UP : in std_logic;     --'0': Count down; '1': count up
     LOAD : in std_logic; --Synchronous load
     DIN : in std_logic_vector(27 downto 0);   --Data to load
     COUNT: out std_logic_vector(27 downto 0);  --count
     ZERO: out std_logic  --Negated '0' value
     );
end decimal_cntr;


architecture Behavioral of decimal_cntr is
	signal reg: unsigned(COUNT'range);
begin
	 process(CLR,CLK)
    begin
    	if CLR='1' then
        	reg<=(others=>'0');
        elsif rising_edge(CLK) then 
        	if CE='1' then
        	   if LOAD='1' then 
        	       reg<= unsigned(DIN);
        	   elsif UP = '1' then
        	       reg <= reg+1;
        	   else
         	       reg <= reg-1;
               end if;
             end if;
         end if;
     
    end process;
    
    COUNT<= std_logic_vector(reg);
    ZERO <= '1' when reg=0 else '0';


end architecture;
