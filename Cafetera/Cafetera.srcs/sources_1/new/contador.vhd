library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

entity contador is
port(
    clk : in std_logic;
    reset : in std_logic;
    count_o : out std_logic_vector(7 downto 0);
    end_o   : out std_logic
);
end entity;

architecture rtl of contador is
    signal cuenta : integer range 0 to 255;

begin
    process(clk,reset)
    begin
    	if (reset='1') then
            cuenta <= 0;
            end_o <= '0';
        elsif(rising_edge(clk)) then
            end_o <= '0';
            if(cuenta=255) then
                cuenta <= 0;
                end_o <= '1';
            else    
                cuenta <= cuenta + 1;
            end if;    
        end if;
    end process;
    
    count_o <= std_logic_vector(to_unsigned(cuenta, 8));

end architecture;

library IEEE;
use IEEE.std_logic_1164.all;

entity test is
end entity;